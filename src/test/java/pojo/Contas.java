package pojo;

public class Contas {
	
	private Integer id;
	private String nome;
	private Boolean visivel;
	private Integer usuario_id;
	
	public Contas(String nome) {
		this.nome = nome;
	}
	
	public Contas() {}
	
	public String getNome() {
		return this.nome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getVisivel() {
		return visivel;
	}

	public void setVisivel(Boolean visivel) {
		this.visivel = visivel;
	}

	public Integer getUsuario_id() {
		return usuario_id;
	}

	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}



}
