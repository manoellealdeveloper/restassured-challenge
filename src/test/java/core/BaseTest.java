package core;

import org.hamcrest.Matchers;
import org.junit.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;

public class BaseTest implements Constants {
	
	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = APP_BASE_URL;
		//RestAssured.port = APP_PORT;
		//RestAssured.basePath = APP_BASE_PATH;
		
		RequestSpecBuilder reqSpec = new RequestSpecBuilder();
		reqSpec.setContentType(APP_CONTENT_TYPE);
		RestAssured.requestSpecification = reqSpec.build();
		
		ResponseSpecBuilder resSpec = new ResponseSpecBuilder();
		resSpec.expectResponseTime(Matchers.lessThan(MAX_TIMEOUT));
		RestAssured.responseSpecification = resSpec.build();
		
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	}

}
