package core;

import static io.restassured.RestAssured.given;

import java.util.HashMap;
import java.util.Map;

import com.github.javafaker.Faker;

import io.restassured.http.ContentType;

public class Utils {

	public static String tokenGenerator() {

		Map<String, String> login = new HashMap<String, String>();
		login.put("email", "rruanandrearagao@yahoo.ca");
		login.put("senha", "123456");

		return given()
					.contentType(ContentType.JSON)
					.body(login)
				.when()
					.post("http://barrigarest.wcaquino.me/signin")
				.then()
					.statusCode(200)
					.extract()
					.path("token")
				;
	}

	public static String nameGenerator() {
		return new Faker().name().firstName() + " " + new Faker().name().lastName();
	}
	
	public static String textGenerator() {
		return new Faker().lorem().characters();
	}

}
