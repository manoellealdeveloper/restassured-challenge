package testSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import testCases.ContasTest;
import testCases.TransacoesTest;

@RunWith(Suite.class)
@SuiteClasses({
	ContasTest.class,
	TransacoesTest.class
})
public class AllTests {

}
