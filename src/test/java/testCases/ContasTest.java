package testCases;

import static org.hamcrest.Matchers.is;

import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Test;

import api.ContasController;
import api.TransacoesController;
import core.BaseTest;
import core.Utils;
import io.restassured.module.jsv.JsonSchemaValidator;
import pojo.Contas;
import pojo.Transacoes;

public class ContasTest extends BaseTest{
		
	@Test
	public void deveValidarGetContaSemToken() {
		new ContasController()
			.getContasWithOutToken()
		    .statusCode(HttpStatus.SC_UNAUTHORIZED);
	}
	
	@Test
	public void deveIncluirConta() {
		String nome = Utils.nameGenerator();
		new ContasController()
			.postContas(new Contas(nome))
			.log().all()
			.statusCode(HttpStatus.SC_CREATED)
			.body("nome", is(nome))
			.body(JsonSchemaValidator.matchesJsonSchemaInClasspath("postContasSuccess.json"))
			;
	}
	
	@Test
	public void deveAlterarConta() {
		String nome = Utils.nameGenerator();
		
		Contas conta = new Contas(nome);
		
		conta = new ContasController()
			.postContas(conta)
			.extract()
			.body()
			.as(Contas.class)
			;
		
		nome = Utils.nameGenerator();
		conta.setNome(nome);
		
		new ContasController()
			.putContas(conta)
			.statusCode(HttpStatus.SC_OK)
			.body("nome", is(nome))
			.body(JsonSchemaValidator.matchesJsonSchemaInClasspath("postContasSuccess.json"))
			;
	}
	
	@Test
	public void naoDevePermitirIncluirNomeRepetido() {
		String nome = Utils.nameGenerator();
		
		new ContasController()
				.postContas(new Contas(nome))
				.statusCode(HttpStatus.SC_CREATED);
		
		new ContasController()
			.postContas(new Contas(nome))
			.statusCode(HttpStatus.SC_BAD_REQUEST)
			.body("error", Matchers.is("Já existe uma conta com esse nome!"))
			;
	}
	
	@Test
	public void naoDeveDeletarContaComMovimentacao() {
		Contas conta = new ContasController()
				.postContas(new Contas(Utils.nameGenerator()))
				.extract()
				.body().as(Contas.class);
			
			Transacoes transacao = new Transacoes(
					conta.getId(),
					conta.getUsuario_id(),
					Utils.textGenerator(),
					Utils.nameGenerator(),
					"REC",
					"25/08/2021",
					"25/08/2021",
					Float.valueOf(10.56f), 
					true
					);
			
			new TransacoesController()
				.postTransacao(transacao)
				;
			
			new ContasController()
				.deleteContas(conta.getId())
				.statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
			;
	}

}
