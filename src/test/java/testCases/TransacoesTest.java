package testCases;

import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Test;

import api.ContasController;
import api.TransacoesController;
import core.BaseTest;
import core.Utils;
import io.restassured.module.jsv.JsonSchemaValidator;
import pojo.Contas;
import pojo.Transacoes;

public class TransacoesTest extends BaseTest{
			
	@Test
	public void deveIncluirTransacao() {
		
		Contas conta = new ContasController()
			.postContas(new Contas(Utils.nameGenerator()))
			.extract()
			.body().as(Contas.class);
		
		Transacoes transacao = new Transacoes(
				conta.getId(),
				conta.getUsuario_id(),
				Utils.textGenerator(),
				Utils.nameGenerator(),
				"REC",
				"25/08/2021",
				"25/08/2021",
				Float.valueOf(10.56f), 
				true
				);
		
		new TransacoesController()
			.postTransacao(transacao)
			.statusCode(HttpStatus.SC_CREATED)
			.body("conta_id", Matchers.is(conta.getId()))
			.body("usuario_id", Matchers.is(conta.getUsuario_id()))
			.body(JsonSchemaValidator.matchesJsonSchemaInClasspath("PostTransacoesSuccess.json"))
			;
	
	}
	
	@Test
	public void deveValidarObrigatoriedadeCampoContaId() {
		Contas conta = new ContasController()
				.postContas(new Contas(Utils.nameGenerator()))
				.extract()
				.body().as(Contas.class);
			
			Transacoes transacao = new Transacoes(
					null,
					conta.getUsuario_id(),
					Utils.textGenerator(),
					Utils.nameGenerator(),
					"REC",
					"25/08/2021",
					"25/08/2021",
					Float.valueOf(10.56f), 
					true
			);
			
			new TransacoesController()
				.postTransacao(transacao)
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.body("[0].param", Matchers.is("conta_id"))
				.body("[0].msg", Matchers.is("Conta é obrigatório"))
			;
	}
	
	@Test
	public void naoDeveCadastrarTransacoesFuturas() {
		Contas conta = new ContasController()
				.postContas(new Contas(Utils.nameGenerator()))
				.extract()
				.body().as(Contas.class);
			
			Transacoes transacao = new Transacoes(
					conta.getId(),
					conta.getUsuario_id(),
					Utils.textGenerator(),
					Utils.nameGenerator(),
					"REC",
					"25/08/2022",
					"25/08/2021",
					Float.valueOf(10.56f), 
					true
			);
			
			new TransacoesController()
				.postTransacao(transacao)
				.log().all()
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.body("[0].param", Matchers.is("data_transacao"))
				.body("[0].msg", Matchers.is("Data da Movimentação deve ser menor ou igual à data atual"))
				.body("[0].value", Matchers.is(transacao.getData_transacao()))
			;
	}
}
