package api;

import static io.restassured.RestAssured.given;

import core.Utils;
import io.restassured.response.ValidatableResponse;
import pojo.Contas;

public class ContasController {
	
	private String token = Utils.tokenGenerator();
			
	public ValidatableResponse getContasWithOutToken() {
		
		return given()
				.when()
				.get("/contas")
				.then();
	}
	
	public ValidatableResponse postContas(Contas conta) {
		
		return given()
					.header("Authorization", "JWT " + token)
					.body(conta)
				.when()
				.post("/contas")
				.then();
	}
	
	public ValidatableResponse putContas(Contas conta) {
				
		return given()
				.header("Authorization", "JWT " + token)
				.body(conta)
				.pathParam("id", conta.getId())
			.when()
			.put("/contas/{id}")
			.then();
		
	}
	
	public ValidatableResponse deleteContas(Integer id) {
		return given()
				.header("Authorization", "JWT " + token)
				.pathParam("id", id)
			.when()
			.delete("/contas/{id}")
			.then();
	}

}
