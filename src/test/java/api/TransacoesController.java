package api;

import static io.restassured.RestAssured.given;

import core.Utils;
import io.restassured.response.ValidatableResponse;
import pojo.Transacoes;

public class TransacoesController {

	private String token = Utils.tokenGenerator();

	public ValidatableResponse postTransacao(Transacoes transacao) {

		return given()
					.header("Authorization", "JWT " + token)
					.body(transacao)
			   .when()
			   		.post("/transacoes")
			   .then();
	}

}
